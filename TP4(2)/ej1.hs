--------------------------------------------------------------------------------
-- Conjunto de funciones del TP4(2) de Paradigmas y Lenguajes - Año 2013.
--------------------------------------------------------------------------------
-- a) swap, que dado un par (a,b) devuelve el par (b,a)

swap :: (x, y) -> (y, x)
swap x = (snd x, fst x)

--------------------------------------------------------------------------------
-- b) apply, que toma una función y un valor, y devuelve el resultado
--    de aplicar la función al valor dado

apply :: (a -> a) -> (a -> a)
apply fn x = fn x

--------------------------------------------------------------------------------
-- c) first, que toma un par ordenado, y devuelve su primera componente

first :: (a, b) -> a
first x = fst x

--------------------------------------------------------------------------------
-- d) sign, la función signo
-- Devuelve:
--  0: si es cero
--  1: si es positivo
-- -1: si es negativo

sign :: Float -> Int
sign x =
  if (x > 0) 
    then 1
    else 
      if (x < 0) 
        then (-1)
        else 0

--------------------------------------------------------------------------------
-- e) abs, la función valor absoluto (usando sign y sin usarla)
-- Nota: La funcion abs ya se encuentra en el nucleo de Haskell por lo
--       cual se definiran "absoluteWithSign" y "absoluteWithoutSign"
--       respectivamente.

absoluteWithSign :: Float -> Float
absoluteWithSign x =
  if (sign x >= 0) 
    then x
    else (-x)

absoluteWithoutSign :: Float -> Float
absoluteWithoutSign x =
  if (x >= 0) 
    then x
    else (-x)
